package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.TeamInfo;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Properties;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);

    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private List<HistoryItem> historyItems;
    private List<HistoryItem> reverseHistoryItems;

    public ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        //SalesSystemDAO dao = new HibernateSalesSystemDAO();
        SalesSystemDAO dao = new InMemorySalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
    }

    /**
     * Run the sales system CLI.
     */
    public void run() throws IOException {
        System.out.println("===========================");
        System.out.println("=       Sales System      =");
        System.out.println("===========================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            System.out.println("_____________________");
            System.out.println("\nDone. ");
        }
    }

    private void showStock() {
        List<StockItem> stockItems = dao.getStockItemList();
        System.out.println("-------------------------");
        for (StockItem si : stockItems) {
            System.out.println(si.getId() + " " + si.getName() + " " + si.getPrice() + "Euro (" + si.getQuantity() + " items)");
        }
        if (stockItems.size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showCart() {
        System.out.println("-------------------------");
        for (SoldItem si : cart.getAll()) {
            System.out.println(si.getName() + " " + si.getPrice() + " Euro(s) (" + si.getQuantity() + " item(s))");
        }
        if (cart.getAll().size() == 0) {
            System.out.println("\tNothing");
        }
        System.out.println("-------------------------");
    }

    private void showHistory() {
        historyItems = dao.getHistoryItemList();
        reverseHistoryItems = new ArrayList<>(historyItems.size());
        for (int i = historyItems.size() - 1; i >= 0; i--) {
            reverseHistoryItems.add(historyItems.get(i));
        }
        System.out.println("-------------------------");
        for (HistoryItem hi : reverseHistoryItems) {
            System.out.println("ID: " + hi.getId() + " Date: " + hi.getPurchaseLocalDate() + " Time: " + hi.getPurchaseLocalTime().toString().substring(0,8) + " Sum: " + hi.getTotalSum());
        }
        System.out.println("-------------------------");
    }

    private void showPurchase(Long id) {
        historyItems = dao.getHistoryItemList();
        System.out.println("-------------------------");
        for (HistoryItem hi : historyItems) {
            if (hi.getId().equals(id)) {
                for (SoldItem si : hi.getSoldItems()) {
                    System.out.println(si.getName() + " " + si.getPrice() + " Euro(s) (" + si.getQuantity() + " item(s))");
                }
            }
        }
        System.out.println("-------------------------");
    }

    private void showNumberOfPurchases(int n) {
        historyItems = dao.getHistoryItemList();
        reverseHistoryItems = new ArrayList<>();
        for (int i = historyItems.size() - 1; i >= historyItems.size() - n; i--) {
            reverseHistoryItems.add(historyItems.get(i));
        }
        System.out.println("-------------------------");
        for (HistoryItem hi : reverseHistoryItems) {
            System.out.println("ID: " + hi.getId() + " Date: " + hi.getPurchaseLocalDate() + " Time: " + hi.getPurchaseLocalTime().toString().substring(0,8) + " Sum: " + hi.getTotalSum());
        }
        System.out.println("-------------------------");
    }

    private void showDates(LocalDate BeginDate, LocalDate EndDate) {
        historyItems = dao.getHistoryItemList();
        reverseHistoryItems = new ArrayList<>(historyItems.size());
        for (int i = historyItems.size() - 1; i >= 0; i--) {
            reverseHistoryItems.add(historyItems.get(i));
        }
        System.out.println("-------------------------");
        for (HistoryItem hi : reverseHistoryItems) {
            if (hi.getPurchaseLocalDate().compareTo(BeginDate) >= 0 && hi.getPurchaseLocalDate().compareTo(EndDate) <= 0) {
                System.out.println("ID: " + hi.getId() + " Date: " + hi.getPurchaseLocalDate() + " Time: " + hi.getPurchaseLocalTime().toString().substring(0,8) + " Sum: " + hi.getTotalSum());
            }
        }
        System.out.println("-------------------------");
    }

    private  void showTeam() throws IOException {
        System.out.println("-------------------------");
        TeamInfo info =  new TeamInfo();
        System.out.println("Team name:\t\t\t"+info.getTeamname());
        System.out.println("Team members:\t\t\t"+info.getMembers());
        System.out.println("Team contact person:\t\t"+info.getContact());
        System.out.println("-------------------------");
    }

    private void printUsage() {
        System.out.println("-------------------------");
        System.out.println("Usage:");
        System.out.println("h\t\tShow this help");
        System.out.println("w\t\tShow warehouse contents");
        System.out.println("c\t\tShow cart contents");
        System.out.println("hi\t\tShow purchase history");
        System.out.println("num NR\t\tShow last NR purchases");
        System.out.println("p IDX\t\tShow details of purchase with index IDX");
        System.out.println("d DD/MM/YYYY DD/MM/YYYY\t\tShow all purchases from the first date to the second date");
        System.out.println("t\t\tDisplay team info");
        System.out.println("a IDX NR \tAdd NR of stock item with index IDX to the cart");
        System.out.println("b\t\tPurchase the shopping cart");
        System.out.println("r\t\tReset the shopping cart");
        System.out.println("-------------------------");
    }

    private void processCommand(String command) throws IOException {
        String[] c = command.split(" ");

        if (c[0].equals("h"))
            printUsage();
        else if (c[0].equals("q"))
            System.exit(0);
        else if (c[0].equals("w"))
            showStock();
        else if (c[0].equals("c"))
            showCart();
        else if (c[0].equals("hi"))
            showHistory();
        else if (c[0].equals("num")) {
            showNumberOfPurchases(Integer.parseInt(c[1]));
        }
        else if (c[0].equals("p")) {
            try {
                showPurchase(Long.parseLong(c[1]));
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        }
        else if (c[0].equals("d")) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d/MM/yyyy");
            String start = c[1];
            String end = c[2];
            LocalDate startDate = LocalDate.parse(start, formatter);
            LocalDate endDate = LocalDate.parse(end, formatter);
            showDates(startDate, endDate);
        }
        else if (c[0].equals("t"))
            showTeam();
        else if (c[0].equals("b"))
            cart.submitCurrentPurchase();
        else if (c[0].equals("r"))
            cart.cancelCurrentPurchase();
        else if (c[0].equals("a") && c.length == 3) {
            try {
                long id = Long.parseLong(c[1]);
                int amount = Integer.parseInt(c[2]);
                StockItem item = dao.findStockItem(id);
                if (item != null && dao.findStockItem(id)!=null) {
                    int quantityToAdd = Math.min(amount, item.getQuantity());
                    cart.addItem(new SoldItem(item, quantityToAdd,id));
                    log.debug("Added item " + item.getName() + ", quantity " + quantityToAdd + ", to cart");
                } else {
                    System.out.println("\nERROR:");
                    System.out.println("\nno stock item with ID: " + id);
                }
            } catch (SalesSystemException | NoSuchElementException e) {
                log.error(e.getMessage(), e);
            }
        }
        else {
            System.out.println("\nERROR:");
            System.out.println("\nunknown command");
        }
    }

}
