# Team <write your team name here>:
1. Kristjan Pühvel	
2. Indrek Künnapas
3. Karl Joosep Kahk

## Homework 1:
https://pastebin.com/NA6tSnWe
## Homework 2:
use case:
https://pastebin.com/R925FjG2
Diagram:
https://imgur.com/a/C4ogETM

Issues are under the bitbucket repo.

## Homework 3:
<Links to the solution>

## Homework 4:
<Links to the solution>

## Homework 5:
<Links to the solution>

## Homework 6:
functional test plan:
https://pastebin.com/rpY5y8NT
5 test cases:
https://pastebin.com/4Cxm3Rfz
Refactoring summary:
https://pastebin.com/YPd4GMMc

## Homework 7:
test results:
https://pastebin.com/5VarJqMR

task1:
https://pastebin.com/PCyGm4Ym

task 2.2
https://pastebin.com/knkwxPNk

We encourage you to use [markdown syntax](https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)