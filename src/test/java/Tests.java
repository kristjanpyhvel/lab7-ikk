import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class Tests {

    InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();
    ShoppingCart shoppingCart = new ShoppingCart(dao);

    //First one needs to pass, otherwise something wrong in test code execution
    @Test
    public void Test1() {
        assertTrue (1==1);
    }

    @Test
    public void testAddingExistingItem() {
        StockItem add = new StockItem();
        SoldItem addable = new SoldItem(add, 1,1);
        shoppingCart.addItem(addable);
        int a = shoppingCart.getAll().get(0).getQuantity();
        SoldItem addablee = shoppingCart.getAll().get(0);
        shoppingCart.addItem(addablee);
        assert(shoppingCart.getAll().get(0).getQuantity() == a + 1);
    }

    @Test
    public void testAddingNewItem (){
        StockItem add = new StockItem();
        SoldItem addable = new SoldItem(add, 1,1);
        shoppingCart.addItem(addable);
        assert(shoppingCart.getAll().contains(addable));
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithNegativeQuantity() {
        StockItem add = new StockItem();
        SoldItem addable = new SoldItem(add, -1,1);
        shoppingCart.addItem(addable);
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithQuantityTooLarge (){
        StockItem add = new StockItem();
        int a = dao.findStockItem(1).getQuantity();
        SoldItem addable = new SoldItem(add, a + 1,1);
        shoppingCart.addItem(addable);
    }

    @Test(expected = SalesSystemException.class)
    public void testAddingItemWithQuantitySumTooLarge() {
        StockItem add = new StockItem();
        int a = dao.findStockItem(1).getQuantity();
        SoldItem addable = new SoldItem(add,  a,1);
        shoppingCart.addItem(addable);
        shoppingCart.addItem(addable);
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity (){
        StockItem add = new StockItem();
        int a = dao.findStockItem(1).getQuantity();
        SoldItem addable = new SoldItem(add, 1,1);
        shoppingCart.addItem(addable);
        shoppingCart.submitCurrentPurchase();
        assert(dao.findStockItem(1).getQuantity() == a - 1);
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        shoppingCart.submitCurrentPurchase();
        dao.beginTransaction();
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem (){
        StockItem add = new StockItem();
        List<SoldItem> soldItems = new ArrayList<>();
        HistoryItem purchase = new HistoryItem(dao.findLastHistoryId()+1);
        SoldItem addable = new SoldItem(add,  1,1);
        purchase.addSoldItem(addable);
        dao.saveHistoryItem(purchase);
        assert(purchase.getSoldItems().contains(addable));
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        HistoryItem purchase = new HistoryItem(dao.findLastHistoryId()+1);
        assertTrue(purchase.getPurchaseLocalTime().compareTo(java.time.LocalTime.now())<1);
    }

    @Test
    public void testCancellingOrder (){
        StockItem add = new StockItem();
        HistoryItem purchase = new HistoryItem(dao.findLastHistoryId()+1);
        SoldItem addable = new SoldItem(add, 1,1);
        shoppingCart.addItem(addable);
        shoppingCart.cancelCurrentPurchase();
        shoppingCart.addItem(addable);
        shoppingCart.submitCurrentPurchase();
        purchase.addSoldItem(addable);
        assert(purchase.getSoldItems().get(0).getQuantity() == 1);
    }

    @Test
    public void testCancellingOrderQuanititesUnchanged (){
        StockItem add = new StockItem();
        int a = dao.findStockItem(1).getQuantity();
        HistoryItem purchase = new HistoryItem(dao.findLastHistoryId()+1);
        SoldItem addable = new SoldItem(add, 1,1);
        shoppingCart.addItem(addable);
        shoppingCart.cancelCurrentPurchase();
        int b = dao.findStockItem(1).getQuantity();
        assertTrue(a == b);
    }

}
