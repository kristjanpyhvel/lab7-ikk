package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.SalesSystemException;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();
    private final List<StockItem> stockitems =  new ArrayList<>();
    private HistoryItem purchase;

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        // verify that warehouse items' quantity remains at least zero or throw an exception: added the verification to commitTransaction instead
        for (SoldItem i : items) {
            if (i.getId().equals(item.getId())) {
                if (item.getQuantity() + i.getQuantity() < 0)
                    throw new SalesSystemException("Quantity of stock item can not be negative!");
                if (item.getQuantity() + i.getQuantity() > dao.findStockItem(item.getId()).getQuantity())
                    throw new SalesSystemException("Quantity of stock item can not be more than in stock!");
                i.setQuantity(item.getQuantity() + i.getQuantity());
                return;
            }else{
                //throw new SalesSystemException("Item with that ID does not exist");
            }
        }

        if (item.getQuantity() < 0)
            throw new SalesSystemException("Quantity of stock item can not be negative!");
        if (item.getQuantity() > dao.findStockItem(item.getId()).getQuantity())
            throw new SalesSystemException("Quantity of stock item can not be more than in stock!");
        items.add(item);
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        // TODO decrease quantities of the warehouse stock

        // note the use of transactions. InMemorySalesSystemDAO ignores transactions
        // but when you start using hibernate in lab5, then it will become relevant.
        // what is a transaction? https://stackoverflow.com/q/974596
        dao.beginTransaction();
        HistoryItem purchase = new HistoryItem(dao.findLastHistoryId()+1);
        try {
            for (SoldItem item : items) {
                StockItem stockItem = dao.findStockItem(item.getId());
                if (stockItem == null)
                    throw new SalesSystemException("Item with the name and ID" + item.getName() +" : "+item.getId() +" was not found");
                if (stockItem.getQuantity() < item.getQuantity())
                    throw new SalesSystemException("Not enough items with the name and ID" + item.getName() +" : "+item.getId());
                stockItem.setQuantity(stockItem.getQuantity() - item.getQuantity());
                dao.saveSoldItem(item);
                purchase.addSoldItem(item);
                purchase.setTotalSum(purchase.getTotalSum() + item.getPrice()*item.getQuantity());
            }
            if(items.size()!=0){
                dao.saveHistoryItem(purchase);

            }


            dao.commitTransaction();
            items.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }
}
