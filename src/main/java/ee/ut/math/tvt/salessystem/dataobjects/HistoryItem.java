package ee.ut.math.tvt.salessystem.dataobjects;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "HISTORYITEM")
public class HistoryItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private List<SoldItem> soldItems;

    @Column(name = "totalSum")
    private double totalSum;
    @Column(name = "purchaseLocalDate")
    private LocalDate purchaseLocalDate;
    @Column(name = "purchaseLocalTime")
    private LocalTime purchaseLocalTime;

    public HistoryItem(Long id, List<SoldItem> soldItems, double totalSum) {
        this.soldItems = soldItems;
        this.totalSum = totalSum;
        this.purchaseLocalDate = LocalDate.now();
        this.purchaseLocalTime = LocalTime.now();
        this.id = id;
    }

    public HistoryItem(Long id) {
        this.soldItems = new ArrayList<>();
        this.totalSum = 0;
        this.purchaseLocalDate = LocalDate.now();
        this.purchaseLocalTime = LocalTime.now();
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<SoldItem> getSoldItems() {
        return soldItems;
    }

    public void setSoldItems(List<SoldItem> soldItems) {
        this.soldItems = soldItems;
    }

    public void addSoldItem(SoldItem soldItem) {
        this.soldItems.add(soldItem);
    }

    public double getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(double totalSum) {
        this.totalSum = totalSum;
    }

    public LocalDate getPurchaseLocalDate() {
        return purchaseLocalDate;
    }

    public void setPurchaseLocalDate(LocalDate purchaseLocalDate) {
        this.purchaseLocalDate = purchaseLocalDate;
    }

    public LocalTime getPurchaseLocalTime() {
        return purchaseLocalTime;
    }

    public void setPurchaseLocalTime(LocalTime purchaseLocalTime) {
        this.purchaseLocalTime = purchaseLocalTime;
    }
}
