package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {

    private final List<StockItem> stockItemList;
    private final List<SoldItem> soldItemList;
    private final List<HistoryItem> historyItemList;

    public InMemorySalesSystemDAO() {
        List<StockItem> items = new ArrayList<StockItem>();
        items.add(new StockItem(1L, "Lays chips", "Potato chips", 11.0, 5,0.1));
        items.add(new StockItem(2L, "Chupa-chups", "Sweets", 8.0, 8,0.1));
        items.add(new StockItem(3L, "Frankfurters", "Beer sausages", 15.0, 12,0.1));
        items.add(new StockItem(4L, "Free Beer", "Student's delight", 0.0, 100, 0.1));
        this.stockItemList = items;
        this.soldItemList = new ArrayList<>();
        this.historyItemList = new ArrayList<>();
    }

    @Override
    public List<StockItem> getStockItemList() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        for (StockItem item : stockItemList) {
            if (item.getId() == id)
                return item;
        }
        return null;
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemList.add(stockItem);
    }

    @Override
    public void saveHistoryItem(HistoryItem historyItem) {
        historyItemList.add(historyItem);
    }

    @Override
    public void beginTransaction() {
    }

    @Override
    public List<SoldItem> getSoldItemList() {
        return soldItemList;
    }

    @Override
    public List<HistoryItem> getHistoryItemList() {
        return historyItemList;
    }

    @Override
    public Long findLastHistoryId() {
        if (historyItemList.equals(new ArrayList<>())) {
            return 0L;
        }
        else {
            return historyItemList.get(historyItemList.size()-1).getId();
        }
    }

    @Override
    public void rollbackTransaction() {
    }

    @Override
    public void commitTransaction() {
    }
}
