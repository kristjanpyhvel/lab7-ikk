package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        HibernateSalesSystemDAO service = new HibernateSalesSystemDAO();

        List<StockItem> stockItems = service.findStockItems();
        List<SoldItem> soldItems = service.findSoldItems();

        echo("\n\n\n\n" +
                " ==================================================================== \n" +
                " =====                                                          ===== \n" +
                " =====                   DATABASE CONTENTS                      ===== \n" +
                " =====                                                          ===== \n" +
                " ==================================================================== \n"
        );

        echo("\n\n\n\n" +
                " ********************************************** \n" +
                " **              STOCKITEMS                    ** \n" +
                " ********************************************** \n" +
                "\n"
        );

        echo("Found " + stockItems.size() + " stockitems:");
        for (StockItem s : stockItems) {
            echo(s);
        }



        service.close();


    }

    private static void echo(Object o) {
        
        System.out.println(o);
    }
}
