

package ee.ut.math.tvt.salessystem.dataobjects;

import ee.ut.math.tvt.salessystem.SalesSystemException;

import javax.persistence.*;


/**
 * Stock item.
 */

@Entity
@Table(name = "STOCKITEM")
public class StockItem<discountprice>
{



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private double price;

    @Column(name = "description")
    private String description;

    @Column(name = "quantity")
    private int quantity;

    @Column(name = "discount")
    private double discount;

    public StockItem() {
    }

    private double discountprice;

    public StockItem(Long id, String name, String desc, double price, int quantity, double discount) {
        if (id <= 0)
            throw new SalesSystemException("Stock item ID must be positive!");
        if (name.isEmpty())
            throw new SalesSystemException("Stock item name can not be empty!");
        if (price < 0.0)
            throw new SalesSystemException("Stock item price can not be negative!");
        if (quantity <= 0)
            throw new SalesSystemException("Stock item quantity must be positive!");
        if(discount<0 || discount>1)
            throw new SalesSystemException("Discount value must be between 0 and 1");
        this.id = id;
        this.name = name;
        this.description = desc;
        this.price = price;
        this.quantity = quantity;
        this.discount = discount;
        this.discountprice = Math.round(price*(1.0-discount));

    }
/*
    public double discountprice = Math.round(price*(1.0-discount));
*/
    public double getDiscountprice() {
        return discountprice;
    }

    public void setDiscountprice(double discountprice) {
        this.discountprice = discountprice;
    }

    public void setDiscount(double discount) {
        if(discount<0.0){
            throw new SalesSystemException("Negative values are not allowed for: Discount");
        }
        if(discount>1.0){
            throw new SalesSystemException("Discount cant be larger than 1.0");
        }
        if(discount<this.discount){
            this.discountprice = Math.round(price*(1.0-discount));
            this.discount =discount;
        }
        this.discount = discount;
        this.discountprice = Math.round(price*(1.0-discount));
    }

    public void setPrice(double price) {
        if (price < 0.0)
            throw new SalesSystemException("Negative values are not allowed for: Price");
        this.price = price;
        this.discount=0.0;
        this.discountprice = Math.round(price*(1.0-discount));
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.isEmpty())
            throw new SalesSystemException("Item name can not be null");
        this.name = name;
    }

    public double getPrice() {
        return price;
    }
/*
    public void setPrice(double price) {
        this.price = price;
    }
*/
    public Long getId() {
        return id;
    }

    public void setId(long id) {
        if (id <= 0)
            throw new SalesSystemException("Negative values are not allowed for: ID");
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        if (quantity < 0)
            throw new SalesSystemException("Negative values are not allowed for: Quantity");
        this.quantity = quantity;
    }

    public double getDiscount() {
        return discount;
    }
/*
    public void setDiscount(double discount) {
        this.discount = discount;
    }
*/
    @Override
    public String toString() {
        return String.format("StockItem{id=%d, name='%s'}", id, name);
    }
}
