package ee.ut.math.tvt.salessystem.dataobjects;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class TeamInfo{
    private final String teamname;
    private final String contact;
    private final String members;
    private final String image;

    public static Properties readProperties(String filename) throws IOException {
        FileInputStream inputStream;
        Properties prop =null;
        try{
            inputStream = new FileInputStream(filename);
            prop = new Properties();
            prop.load(inputStream);
        }
        catch (FileNotFoundException notfound){
            System.out.println("Didnt find the file :c");
        } catch (IOException ioe){
            System.out.println("Something went wrong");
        }
        return prop;
    }

    public TeamInfo() throws IOException {
        Properties prop = readProperties("../src/main/resources/application.properties");
        this.teamname = prop.getProperty("TeamName");
        this.contact = prop.getProperty("TeamContact");
        this.members = prop.getProperty("TeamMembers");
        this.image = prop.getProperty("TeamLogo");
    }

    public String getTeamname() {
        return teamname;
    }
    public String getContact() {
        return contact;
    }
    public String getMembers() {
        return members;
    }
    public String getImage() {
        return image;
    }
}
