package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.HistoryItem;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController implements Initializable {

    private final SalesSystemDAO dao;
    private boolean ShowAll = true;
    private boolean isTimerOn = true;
    private LocalDate BeginDate;
    private LocalDate EndDate;
    private List<HistoryItem> HistoryList;
    private List<HistoryItem> ReverseHistoryList;

    @FXML
    private TableView<HistoryItem> historyTable;
    @FXML
    private TableColumn<HistoryItem, LocalDate> historyLocalDateColumn;
    @FXML
    private TableColumn<HistoryItem, LocalTime> historyLocalTimeColumn;
    @FXML
    private TableColumn<HistoryItem, Double> historyTotalSumColumn;
    @FXML
    private TableView<SoldItem> soldItemsTable;
    @FXML
    private TableColumn<SoldItem, Long> soldItemIdColumn;
    @FXML
    private TableColumn<SoldItem, String> soldItemNameColumn;
    @FXML
    private TableColumn<SoldItem, Double> soldItemPriceColumn;
    @FXML
    private TableColumn<SoldItem, Integer> soldItemQuantityColumn;
    @FXML
    private DatePicker addBeginDate;
    @FXML
    private DatePicker addEndDate;



    public HistoryController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        historyTable.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {

                    if (newValue != null) {
                        HistoryItem selection = historyTable.getSelectionModel().getSelectedItem();
                        soldItemsTable.setItems(FXCollections.observableList(selection.getSoldItems()));

                    }
                });
        refreshHistoryItems();
        startRefreshTimer();
    }

    public void refreshHistoryItems() {
        if (isTimerOn) {
            HistoryList = dao.getHistoryItemList();
            ReverseHistoryList = new ArrayList<>(HistoryList.size());
            if (ShowAll) {
                for (int i = HistoryList.size() - 1; i >= 0; i--) {
                    ReverseHistoryList.add(HistoryList.get(i));
                }
            } else {
                try {
                    for (int i = HistoryList.size() - 1; i >= HistoryList.size() - 10; i--) {
                        ReverseHistoryList.add(HistoryList.get(i));
                    }
                } catch (Exception e) {
                    throw e;
                }

            }
            historyTable.setItems(FXCollections.observableList(ReverseHistoryList));
        }
    }

    public void startRefreshTimer() {
        Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                refreshHistoryItems();
            }
        }, 0, 2000);
    }

    @FXML
    public void showDateRangeButtonClicked() {
        showDateRange();
    }

    public void showDateRange() {
        isTimerOn = false;
        try {
            BeginDate = this.addBeginDate.getValue();
            EndDate = this.addEndDate.getValue();
            HistoryList = dao.getHistoryItemList();
            ReverseHistoryList = new ArrayList<>(HistoryList.size());
            for (int i = HistoryList.size() - 1; i >= 0; i--) {
                if (HistoryList.get(i).getPurchaseLocalDate().compareTo(BeginDate) >= 0 && HistoryList.get(i).getPurchaseLocalDate().compareTo(EndDate) <= 0) {
                    ReverseHistoryList.add(HistoryList.get(i));
                }
            }
            historyTable.setItems(FXCollections.observableList(ReverseHistoryList));
        } catch (Exception e) {
            throw e;
        }
    }

    @FXML
    public void showLast10ButtonClicked() {
        showLast10();
    }

    public void showLast10() {
        isTimerOn = true;
        ShowAll = false;
        refreshHistoryItems();
    }

    @FXML
    public void showAllButtonClicked() {
        showAll();
    }

    public void showAll() {
        isTimerOn = true;
        ShowAll = true;
        refreshHistoryItems();
    }
}
