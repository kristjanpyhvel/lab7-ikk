package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.dataobjects.SoldItem;
import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.util.Callback;
import javafx.util.StringConverter;

import javax.persistence.Id;
import java.awt.*;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Objects;
import java.util.ResourceBundle;

public class StockController implements Initializable {

    private final SalesSystemDAO dao;

    @FXML
    private Button addItem;
    @FXML
    private TableView<StockItem> warehouseTableView;
    @FXML
    private TextField addItemAmount;
    @FXML
    private TextField addItemName;
    @FXML
    private TextField addItemPrice;
    @FXML
    private TableColumn<StockItem, Long> warehouseIdColumn;
    @FXML
    private TableColumn<StockItem, Double> warehousePriceColumn;
    @FXML
    private TableColumn<StockItem, String> warehouseNameColumn;
    @FXML
    private TableColumn<StockItem, Integer> warehouseQuantityColumn;
    @FXML
    private TableColumn<StockItem, Double> warehouseDiscountColumn;
    @FXML
    private TextField addItemDiscount;
    @FXML
    private Label soldCount;
    @FXML
    private TextField addItemID;

    public StockController(SalesSystemDAO dao) {
        this.dao = dao;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        refreshStockItems();   /*ei saanud automaatset refreshi veel tööle, Indrek*/
        lowstockHighlighterCall();

        this.addItemID.focusedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
                if (!newPropertyValue) {
                    fillAddItemFields();
                }
            }
        });


        warehousePriceColumn.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Double>() {
            @Override
            public String toString(Double price) {
                return Double.toString(price);
            }
            @Override
            public Double fromString(String price) {
                return Double.parseDouble(price);
            }
        }));
        warehousePriceColumn.setOnEditCommit(event -> {
            try {
                event.getRowValue().setPrice(event.getNewValue());
            } catch (Exception e) {
                new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK).showAndWait();
            }
            event.getTableView().refresh();
        });
        warehouseDiscountColumn.setCellFactory(TextFieldTableCell.forTableColumn(new StringConverter<Double>() {
            @Override
            public String toString(Double price) {
                return Double.toString(price);
            }
            @Override
            public Double fromString(String price) {
                return Double.parseDouble(price);
            }
        }));


        warehouseDiscountColumn.setOnEditCommit(event -> {
            try {
                event.getRowValue().setDiscount(event.getNewValue());
            } catch (Exception e) {
                new Alert(Alert.AlertType.ERROR, e.getMessage(), ButtonType.OK).showAndWait();
            }
            event.getTableView().refresh();
        });



    }


    @FXML
    public void refreshButtonClicked() {
        refreshStockItems();
    }

    private void refreshStockItems() {
        warehouseTableView.setItems(FXCollections.observableList(dao.getStockItemList()));
        warehouseTableView.refresh();
        setTotalSoldCall();
    }

    @FXML
    public void removeButtonClicked(){
        removeItem();

    }
    private void removeItem(){
        int id = warehouseTableView.getSelectionModel().getSelectedIndex();
        warehouseTableView.getItems().remove(id);

    }
    private void setTotalSoldCall(){
        setTotalSold();
    }
    @FXML
    public void setTotalSold(){
        int count =0;
        for (int i = 0; i < dao.getSoldItemList().size(); i++) {
            count=count+dao.getSoldItemList().get(i).getQuantity();
        }
        soldCount.setText(String.valueOf(count));
    }
    @FXML
    public void lowstockHighlighterCall() {
        lowstockHighlighter();
    }

    private void lowstockHighlighter() {
        warehouseTableView.setRowFactory(tv -> new TableRow<StockItem>() {
            @Override
            protected void updateItem(StockItem item, boolean empty) {
                super.updateItem(item, empty);
                if (item == null)
                    setStyle("");
                else if (item.getQuantity() > 10)
                    setStyle("-fx-background-color: #baffba;");
                else if (item.getQuantity() < 10)
                    setStyle("-fx-background-color: #ffd7d1;");
                else
                    setStyle("");
            }
        });
    }

    @FXML
    public void addButtonClicked(){
        addItem();
    }



    private void addItem(){
        try {
            if (!addItemAmount.getText().isBlank() && !addItemName.getText().isBlank() && !addItemPrice.getText().isBlank()) {
                DecimalFormat df = new DecimalFormat("0.00");
                int amount = Integer.parseInt(addItemAmount.getText());
                String name = addItemName.getText();
                double price = Double.parseDouble(addItemPrice.getText());
                Long id = Long.parseLong(addItemID.getText());
                ObservableList<StockItem> stock = warehouseTableView.getItems();
                StockItem addable = new StockItem(id, name, "", price, amount,0);
                if (dao.findStockItem(addable.getId()) != null) {
                    for (StockItem i : stock) {
                        if (i.getId().equals(addable.getId())) {
                            i.setQuantity(i.getQuantity() + addable.getQuantity());
                            refreshStockItems();
                        }
                    }
                } else {
                    dao.saveStockItem(addable);
                    refreshStockItems();
                }

            } else {
                throw new Exception("Fill all fields!");
            }
        }catch (Exception e){
            new Alert(Alert.AlertType.ERROR,e.getMessage(),ButtonType.OK).showAndWait();
        }
    }

    private void disableItemFields(boolean bool){
        addItemPrice.setDisable(bool);
        addItemName.setDisable(bool);
        addItemDiscount.setDisable(bool);

    }
    private StockItem getStockItemByID(){
        try {
            long code = Long.parseLong(addItemID.getText());
            return dao.findStockItem(code);
        } catch (NumberFormatException e) {
            return null;
        }
    }
    private void fillAddItemFields(){
        try{
            StockItem stockItem =  getStockItemByID();
            if(stockItem!=null){
                addItemPrice.setText(String.valueOf(stockItem.getPrice()));
                addItemName.setText(stockItem.getName());
                addItemDiscount.setText(String.valueOf(stockItem.getDiscount()));
                disableItemFields(true);
            }
            else {
                addItemName.setText("");
                addItemPrice.setText("");
                addItemDiscount.setText("");
               disableItemFields(false);
            }
        }catch(Exception e){}
    }
    @FXML
    public void fillAddItemFieldsCall(){
        fillAddItemFields();
    }
}
