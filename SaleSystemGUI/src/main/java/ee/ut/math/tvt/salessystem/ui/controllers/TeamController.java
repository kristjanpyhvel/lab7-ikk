package ee.ut.math.tvt.salessystem.ui.controllers;


import ee.ut.math.tvt.salessystem.dataobjects.StockItem;
import ee.ut.math.tvt.salessystem.dataobjects.TeamInfo;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;

public class TeamController implements Initializable {
    @FXML
    private Label teamname;

    @FXML
    private Label contactperson;

    @FXML
    private Label teammembers;
    @FXML
    private ImageView teamlogo;

    @FXML
    private void addProperties() throws IOException {
        TeamInfo info = new TeamInfo();
        Image teamimg = new Image(info.getImage());
        teamname.setText(info.getTeamname());
        contactperson.setText(info.getContact());
        teammembers.setText(info.getMembers());
        teamlogo.setImage(teamimg);

    }
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            addProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
        // TODO refresh view after adding new items
    }




}
